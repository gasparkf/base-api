package io.pismo.util;

import java.util.Base64;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

public class SegurancaUtil {
	
	public static void autenticou(RoutingContext ctx, Handler<Boolean> handler){
		String aut = ctx.request().headers().get("Authorization");
		if(aut == null){
			RespostaUtil.respostaErro(401, ctx);
			handler.handle(false);
		}else{
			String resultado = new String(Base64.getDecoder().decode(aut.substring(6)));
			if(!resultado.equals("admin:admin")){
				RespostaUtil.respostaErro(401, ctx);
				handler.handle(false);
			}else{
				handler.handle(true);
			}
		}
		
	}
	
	

}
