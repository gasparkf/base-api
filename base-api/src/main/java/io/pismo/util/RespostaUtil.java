package io.pismo.util;

import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;

public class RespostaUtil {
	
	public static void respostaErro(Integer codigo, RoutingContext ctx){
		ctx.response()
		.putHeader("content-type", "application/json; charset=utf-8")
		.setStatusCode(codigo).end();
		
	}
	
	public static void respostaErro(Integer codigo, RoutingContext ctx, Object json){
		ctx.response()
		.putHeader("content-type", "application/json; charset=utf-8")
		.setStatusCode(codigo)
		.end(json.toString());
		
	}
	
	public static void respostaErroEP(Integer codigo, RoutingContext ctx, Object json){
		ctx.response()
		.putHeader("content-type", "application/json; charset=utf-8")
		.setStatusCode(codigo)
		.end(Json.encodePrettily(json));
		
	}

}
