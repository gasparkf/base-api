package io.pismo.base.entidade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Venda {
	
	private Long id;
	
	private Date data;
	
	private Double total = 0d;
	
	public Venda(){
		
	}
	
	public Venda(Long id, Date data, Double total, List<Produto> produtos) {
		super();
		this.id = id;
		this.data = data;
		this.total = total;
		this.produtos = produtos;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	private List<Produto> produtos = new ArrayList<Produto>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	

}
