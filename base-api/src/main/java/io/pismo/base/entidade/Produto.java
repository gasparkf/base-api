package io.pismo.base.entidade;

import io.vertx.core.json.JsonObject;

public class Produto {
	
	private Long id;
	
	private String descricao;
	
	private String categoria;
	
	private Double valor = 0D;

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Produto(){
		
	}
	
	public Produto(Long id, String descricao, String categoria, Double valor) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.categoria = categoria;
		this.valor = valor;
	}
	
	public Produto(String descricao, String categoria, Double valor) {
		super();
		this.descricao = descricao;
		this.categoria = categoria;
		this.valor = valor;
	}	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	public JsonObject toJson() {
		JsonObject json = new JsonObject()
				.put("descricao", descricao)
				.put("categoria", categoria);
		if (id != null && id > 0) {
			json.put("id", id);
		}
		return json;
	}

}
