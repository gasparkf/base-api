package io.pismo.base.dao;

import java.io.IOException;
import java.util.List;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;

public class DAO {

	public static JDBCClient jdbcClient;

	public static void iniciar(Vertx vertx, String url, String user, String driver_class, String password, int max_pool_size) throws IOException {
		jdbcClient = JDBCClient.createShared(vertx, new JsonObject()
				.put("url", url)
				.put("driver_class", driver_class)
				.put("user", user)
				.put("password", password)
				.put("max_pool_size", max_pool_size));
	}

	public static void gravar(String sql, JsonArray params, Handler<Long> handler) {
		jdbcClient.getConnection(ar -> {
			if(ar.failed()){
				throw new RuntimeException("Conex�o falhou:" + ar.cause().getLocalizedMessage());
			}else {
				SQLConnection connection = ar.result();
				connection.updateWithParams(sql, params, res -> {
					if (res.failed()) {
						throw new RuntimeException("Erro SQL:" + res.cause().getLocalizedMessage());
					}
					handler.handle(res.result().getKeys().getLong(0));
				});
				connection.close();
			}
		});
	}
	
	public static void gravar(String sql, Handler<Void> handler) {
		jdbcClient.getConnection(ar -> {
			if(ar.failed()){
				throw new RuntimeException("Conex�o falhou:" + ar.cause().getLocalizedMessage());
			}else {
				SQLConnection connection = ar.result();
				connection.update(sql, res -> {
					if (res.failed()) {
						throw new RuntimeException("Erro SQL:" + res.cause().getLocalizedMessage());
					}
				});
				connection.close();
			}
		});
	}
	
	public static void buscarTodos(String sql, Handler<List<JsonObject>> handler) {
		jdbcClient.getConnection(ar -> {
			if(ar.failed()){
				throw new RuntimeException("Conex�o falhou:" + ar.cause().getLocalizedMessage());
			}else {
				SQLConnection connection = ar.result();
				connection.query(sql, res -> {
					if (res.failed()) {
						throw new RuntimeException("Erro SQL:" + res.cause().getLocalizedMessage());
					} else {
						handler.handle(res.result().getRows());
					}
				});
				connection.close();
			}
		});
	}
	
	public static void buscarTodos(String sql, JsonArray params, Handler<List<JsonObject>> handler) {
		jdbcClient.getConnection(ar -> {
			if(ar.failed()){
				throw new RuntimeException("Conex�o falhou:" + ar.cause().getLocalizedMessage());
			}else {
				SQLConnection connection = ar.result();
				connection.queryWithParams(sql, params, res -> {
					if (res.failed()) {
						throw new RuntimeException("Erro SQL:" + res.cause().getLocalizedMessage());
					} else {
						handler.handle(res.result().getRows());
					}
				});
				connection.close();
			}
		});
	}
}
